################################################################################
#
# mali-opengles-sdk
#
################################################################################

MALI_HEADERS_VERSION = v2.4.4.ef7d5a
MALI_HEADERS_SOURCE = Mali_OpenGL_ES_SDK_$(MALI_HEADERS_VERSION)_Linux_x64.tar.gz
MALI_HEADERS_SITE = https://developer.arm.com/-/media/Files/downloads/mali-sdk/v2.4.4

MALI_HEADERS_INSTALL_STAGING = YES

MALI_HEADERS_DEPENDENCIES = mali-openvg-headers

MALI_HEADERS_STAGING_DIR=$(STAGING_DIR)/usr/include

define MALI_HEADERS_INSTALL_STAGING_CMDS
	@mkdir -p $(MALI_HEADERS_STAGING_DIR)
	@cp -r $(@D)/simple_framework/inc/mali/EGL $(MALI_HEADERS_STAGING_DIR)
endef

define MALI_HEADERS_INSTALL_TARGET_CMDS
endef

$(eval $(generic-package))
