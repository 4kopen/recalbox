################################################################################
#
# rpi-userland
#
################################################################################

MALI_OPENVG_HEADERS_VERSION = f0642e3b58d8a140a3f7621630c15fbfa794b19d
MALI_OPENVG_HEADERS_SITE = $(call github,raspberrypi,userland,$(MALI_OPENVG_HEADERS_VERSION))
MALI_OPENVG_HEADERS_LICENSE = BSD-3c
MALI_OPENVG_HEADERS_LICENSE_FILES = LICENCE
MALI_OPENVG_HEADERS_INSTALL_STAGING = YES

MALI_OPENVG_HEADERS_STAGING_DIR=$(STAGING_DIR)/usr/include
define MALI_OPENVG_HEADERS_INSTALL_STAGING_CMDS
	@mkdir -p $(MALI_OPENVG_HEADERS_STAGING_DIR)
	@cp -r $(@D)/interface/khronos/include/VG $(MALI_OPENVG_HEADERS_STAGING_DIR)
endef

define MALI_OPENVG_HEADERS_INSTALL_TARGET_CMDS
endef


$(eval $(generic-package))
